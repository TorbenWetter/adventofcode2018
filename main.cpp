#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include <sstream>
#include <stdio.h>
#include <ctime>
#include <unordered_map>
#include <cstring>
#include <climits>
#include <math.h>
#include <set>
#include <regex>
#include <list>

using namespace std;

void day1() {
    ifstream inputFile("day1.txt");
    if (inputFile.is_open()) {
        // Part 1
        vector<int> frequencyChanges;
        int frequencySum = 0;
        for (string line; getline(inputFile, line);) {
            int freqChange = stoi(line);
            frequencyChanges.push_back(freqChange);
            frequencySum += freqChange;
        }
        inputFile.close();
        cout << frequencySum << endl;

        // Part 2
        unordered_set<int> frequencies = {0};
        int lastFrequency = 0;
        bool done = false;
        while (!done) {
            for (int frequencyChange : frequencyChanges) {
                int newFrequency = lastFrequency + frequencyChange;
                if (frequencies.find(newFrequency) != frequencies.end()) {
                    cout << newFrequency << endl;
                    done = true;
                    break;
                }
                frequencies.insert(newFrequency);
                lastFrequency = newFrequency;
            }
        }
    }
}

void day2() {
    ifstream inputFile("day2.txt");
    if (inputFile.is_open()) {
        // Part 1
        unordered_set<string> boxIds;
        int twiceAmount = 0, threeTimesAmount = 0;
        for (string boxId; getline(inputFile, boxId);) {
            bool twice = false, threeTimes = false;
            for (const char &c : boxId) {
                int amount = (int) count(boxId.begin(), boxId.end(), c);
                if (amount == 2) {
                    twice = true;
                } else if (amount == 3) {
                    threeTimes = true;
                }
            }
            twiceAmount += twice;
            threeTimesAmount += threeTimes;

            boxIds.insert(boxId);
        }
        inputFile.close();
        cout << twiceAmount * threeTimesAmount << endl;

        // Part 2
        int idLength = (int) (*boxIds.begin()).length();
        for (const string &boxId1 : boxIds) {
            for (const string &boxId2 : boxIds) {
                if (boxId1 == boxId2) {
                    continue;
                }

                int differingCharAmount = 0;
                for (int i = 0; i < idLength; i++) {
                    if (boxId1[i] != boxId2[i]) {
                        differingCharAmount++;
                        if (differingCharAmount > 1) {
                            break;
                        }
                    }
                }

                if (differingCharAmount == 1) {
                    string commonChars;
                    for (int i = 0; i < idLength; i++) {
                        if (boxId1[i] == boxId2[i]) {
                            commonChars += boxId1[i];
                        }
                    }
                    cout << commonChars << endl;
                    goto break_out;
                }
            }
        }
        break_out:
        {};
    }
}

struct claim {
    int id, x, y, width, height;
};

void day3() {
    ifstream inputFile("day3.txt");
    if (inputFile.is_open()) {
        unsigned long vectorSize = 1000;
        vector<vector<int>> overlaps(vectorSize, vector<int>(vectorSize, 0));
        vector<claim> claims;
        for (string claimString; getline(inputFile, claimString);) {
            struct claim c{};
            sscanf(claimString.c_str(), "#%d @ %d,%d: %dx%d", &c.id, &c.x, &c.y, &c.width, &c.height);

            for (int _x = c.x; _x < c.x + c.width; _x++) {
                for (int _y = c.y; _y < c.y + c.height; _y++) {
                    overlaps[_x][_y]++;
                }
            }

            claims.push_back(c);
        }
        inputFile.close();

        // Part 1
        int overlapAmount = 0;
        for (const vector<int> &v : overlaps) {
            overlapAmount += (int) count_if(v.begin(), v.end(), [](int n) { return n > 1; });
        }
        cout << overlapAmount << endl;

        // Part 2
        for (const claim &c : claims) {
            int x1 = c.x, y1 = c.y, x2 = x1 + c.width, y2 = y1 + c.height;
            bool noOverlap = true;
            for (int _x = x1; _x < x2; _x++) {
                for (int _y = y1; _y < y2; _y++) {
                    if (overlaps[_x][_y] != 1) {
                        noOverlap = false;
                        goto break_out;
                    }
                }
            }
            break_out:
            {};
            if (noOverlap) {
                cout << c.id << endl;
                break;
            }
        }
    }
}

struct shiftInfo {
    int year, month, day, hour, minute;
    char event[32];
};

bool shiftInfoSorter(const shiftInfo &lhs, const shiftInfo &rhs) {
    if (lhs.year != rhs.year)
        return lhs.year < rhs.year;
    if (lhs.month != rhs.month)
        return lhs.month < rhs.month;
    if (lhs.day != rhs.day)
        return lhs.day < rhs.day;
    if (lhs.hour != rhs.hour)
        return lhs.hour < rhs.hour;
    return lhs.minute < rhs.minute;
}

tm make_time(const int &year, const int &month, const int &day, const int &hour, const int &minute) {
    tm tm = {0};
    tm.tm_year = year + 452 - 1900; // +452 to get to 1970 (leap year; better than 1518) | -1900 because c++ year count
    tm.tm_mon = month - 1; // month count starts at 0 (January)
    tm.tm_mday = day;
    tm.tm_hour = hour;
    tm.tm_min = minute;
    return tm;
}

bool intVectorSorter(const pair<int, vector<int>> &p1, const pair<int, vector<int>> &p2) {
    return p1.second.size() < p2.second.size();
}

bool intPairSorter(const pair<int, int> &p1, const pair<int, int> &p2) {
    return p1.second < p2.second;
}

pair<int, int> maxIntValuePair(unordered_map<int, int> map) {
    auto maxElement = max_element(map.begin(), map.end(), &intPairSorter);
    return make_pair(maxElement->first, maxElement->second);
}

bool intWithIntPairSorter(const pair<int, pair<int, int>> &p1, const pair<int, pair<int, int>> &p2) {
    return p1.second.second < p2.second.second;
}

pair<int, pair<int, int>> maxIntPairValuePair(unordered_map<int, pair<int, int>> map) {
    auto maxElement = max_element(map.begin(), map.end(), &intWithIntPairSorter);
    return make_pair(maxElement->first, maxElement->second);
}

void day4() {
    ifstream inputFile("day4.txt");
    if (inputFile.is_open()) {
        vector<shiftInfo> shiftInfos;
        for (string shiftString; getline(inputFile, shiftString);) {
            struct shiftInfo si{};
            sscanf(shiftString.c_str(), "[%d-%d-%d %d:%d] %31[0-9a-zA-Z# ]", &si.year, &si.month, &si.day, &si.hour,
                   &si.minute, si.event);

            shiftInfos.push_back(si);
        }
        inputFile.close();

        sort(shiftInfos.begin(), shiftInfos.end(), &shiftInfoSorter);

        // Part 1
        unordered_map<int, vector<int>> sleepMinutes;
        int currentGuardID = -1;
        tm sleepBegin{};
        time_t beginTime = -1;
        for (const shiftInfo &si : shiftInfos) {
            string event = si.event;
            if (event.rfind("Guard ", 0) == 0) {
                sscanf(event.c_str(), "Guard #%d begins shift", &currentGuardID);
            } else if (event == "falls asleep") {
                sleepBegin = make_time(si.year, si.month, si.day, si.hour, si.minute);
            } else if (event == "wakes up") {
                tm sleepEnd = make_time(si.year, si.month, si.day, si.hour, si.minute);
                for (int minute = sleepBegin.tm_min; minute < sleepEnd.tm_min; minute++) {
                    if (!sleepMinutes.count(currentGuardID)) {
                        vector<int> v;
                        sleepMinutes[currentGuardID] = v;
                    }
                    sleepMinutes[currentGuardID].push_back(minute);
                }
            }
        }

        auto maxSleep = max_element(sleepMinutes.begin(), sleepMinutes.end(), &intVectorSorter);
        int maxSleepID = maxSleep->first;
        vector<int> maxSleepMinutes = maxSleep->second;

        unordered_map<int, int> minuteAmounts;
        for (const int &minute : maxSleepMinutes) {
            if (!minuteAmounts.count(minute)) {
                minuteAmounts[minute] = 0;
            }
            minuteAmounts[minute] += 1;
        }
        pair<int, int> maxMinute = maxIntValuePair(minuteAmounts);
        int maxSleepMinute = maxMinute.first;
        cout << maxSleepID * maxSleepMinute << endl;

        // Part 2
        unordered_map<int, pair<int, int>> maxGuardMinutes;
        for (const auto &sleep : sleepMinutes) {
            int guardID = sleep.first;
            vector<int> guardSleepMinutes = sleep.second;

            unordered_map<int, int> guardMinuteAmounts;
            for (const int &minute : guardSleepMinutes) {
                if (!guardMinuteAmounts.count(minute)) {
                    guardMinuteAmounts[minute] = 0;
                }
                guardMinuteAmounts[minute] += 1;
            }
            pair<int, int> maxGuardMinute = maxIntValuePair(guardMinuteAmounts);
            int maxGuardSleepMinute = maxGuardMinute.first, maxGuardSleepMinuteAmount = maxGuardMinute.second;

            maxGuardMinutes[guardID] = make_pair(maxGuardSleepMinute, maxGuardSleepMinuteAmount);
        }
        pair<int, pair<int, int>> maxGuardTotalMinute = maxIntPairValuePair(maxGuardMinutes);
        int maxGuardID = maxGuardTotalMinute.first;
        int maxGuardMinute = maxGuardTotalMinute.second.first;
        cout << maxGuardID * maxGuardMinute << endl;
    }
}

long reactedPolymerSize(vector<char> polymer) {
    bool changeMade;
    do {
        changeMade = false;
        char c1 = polymer[0];
        for (int i = 1; i < polymer.size(); i++) {
            char c2 = polymer[i];
            if (tolower(c1) != tolower(c2) || c1 == c2) {
                c1 = c2;
                continue;
            }

            polymer.erase(polymer.begin() + (i - 1), polymer.begin() + (i + 1));
            changeMade = true;
            break;
        }
    } while (changeMade);
    return polymer.size();
}

string toLower(string text) {
    transform(text.begin(), text.end(), text.begin(), ::tolower);
    return text;
}

vector<char> removeLetters(vector<char> v, const char &letter) {
    v.erase(remove(v.begin(), v.end(), letter), v.end());
    v.erase(remove(v.begin(), v.end(), toupper(letter)), v.end());
    return v;
}

void day5() {
    ifstream inputFile("day5.txt");
    if (inputFile.is_open()) {
        string polymerString;
        getline(inputFile, polymerString);
        inputFile.close();

        // Part 1
        vector<char> polymer(polymerString.begin(), polymerString.end());
        cout << reactedPolymerSize(polymer) << endl;

        // Part 2
        string lowerPolymerString = toLower(polymerString);
        long bestReactedSize = polymerString.length();
        unordered_set<char> uniqueChars(lowerPolymerString.begin(), lowerPolymerString.end());
        for (const char &removedC : uniqueChars) {
            vector<char> newPolymer = removeLetters(polymer, removedC);
            long newReactedSize = reactedPolymerSize(newPolymer);
            if (newReactedSize < bestReactedSize) {
                bestReactedSize = newReactedSize;
            }
        }
        cout << bestReactedSize << endl;
    }
}

class Coordinate {

private:
    int x, y;

public:
    Coordinate(int x, int y) : x(x), y(y) {}

    int manhattanDistanceTo(int x, int y) const {
        return abs(this->x - x) + abs(this->y - y);
    }
};

int getIndexOfClosestCoordinate(const int &x, const int &y, const vector<Coordinate> &coordinates) {
    int smallestDistance = INT_MAX, smallestIndex = -1;
    for (int i = 0; i < coordinates.size(); i++) {
        const Coordinate &c = coordinates[i];

        int distance = c.manhattanDistanceTo(x, y);
        if (distance < smallestDistance) {
            smallestDistance = distance;
            smallestIndex = i;
        } else if (distance == smallestDistance) {
            smallestIndex = -1;
        }
    }
    return smallestIndex;
}

void day6() {
    ifstream inputFile("day6.txt");
    if (inputFile.is_open()) {
        int smallestX = INT_MAX, largestX = INT_MIN, smallestY = INT_MAX, largestY = INT_MIN;
        vector<Coordinate> coordinates;
        for (string coordinateString; getline(inputFile, coordinateString);) {
            int x, y;
            sscanf(coordinateString.c_str(), "%d, %d", &x, &y);
            coordinates.emplace_back(x, y);
            if (x < smallestX) {
                smallestX = x;
            } else if (x > largestX) {
                largestX = x;
            }
            if (y < smallestY) {
                smallestY = y;
            } else if (y > largestY) {
                largestY = y;
            }
        }
        inputFile.close();

        // Part 1
        unordered_set<int> infiniteCoordinateIndices;
        for (int x = smallestX - 1; x < largestX + 1; x++) {
            infiniteCoordinateIndices.insert(getIndexOfClosestCoordinate(x, smallestY - 1, coordinates));
        }
        for (int y = smallestY - 1; y < largestY + 1; y++) {
            infiniteCoordinateIndices.insert(getIndexOfClosestCoordinate(largestX + 1, y, coordinates));
        }
        for (int x = largestX + 1; x > smallestX - 1; x--) {
            infiniteCoordinateIndices.insert(getIndexOfClosestCoordinate(x, largestY + 1, coordinates));
        }
        for (int y = largestY + 1; y > smallestY - 1; y--) {
            infiniteCoordinateIndices.insert(getIndexOfClosestCoordinate(smallestX - 1, y, coordinates));
        }
        infiniteCoordinateIndices.erase(-1);

        unordered_map<int, int> closestAmounts;
        for (int i = 0; i < coordinates.size(); i++) {
            closestAmounts.insert(make_pair(i, 0));
        }

        for (int x = smallestX; x <= largestX; x++) {
            for (int y = smallestY; y <= largestY; y++) {
                int closestIndex = getIndexOfClosestCoordinate(x, y, coordinates);
                if (closestIndex != -1) {
                    closestAmounts[closestIndex]++;
                }
            }
        }

        for (const int &infiniteCoordIndex : infiniteCoordinateIndices) {
            closestAmounts.erase(infiniteCoordIndex);
        }

        pair<int, int> maxPair = maxIntValuePair(closestAmounts);
        cout << maxPair.second << endl;

        // Part 2
        int safeDistance = 10000, tolerance = (int) ceil(safeDistance / coordinates.size());
        int desiredLocationAmount = 0;
        for (int x = smallestX - tolerance; x <= largestX + tolerance; x++) {
            for (int y = smallestY - tolerance; y <= largestY + tolerance; y++) {
                int totalDistance = 0;
                for (const Coordinate &c : coordinates) {
                    totalDistance += c.manhattanDistanceTo(x, y);
                }
                if (totalDistance < safeDistance) {
                    desiredLocationAmount++;
                }
            }
        }
        cout << desiredLocationAmount << endl;
    }
}

bool charVectorContains(const vector<char> &v, const char &c) {
    return find(v.begin(), v.end(), c) != v.end();
}

bool charUnorderedSetContains(const unordered_set<char> &set1, const char &c) {
    return set1.find(c) != set1.end();
}

set<char> getAvailableSteps(const unordered_map<char, vector<char>> &dependencies, const vector<char> &chosenLetters,
                            const unordered_set<char> &workingOn = {}) {
    set<char> available;
    for (const auto &dependency : dependencies) {
        char step = dependency.first;
        vector<char> neededSteps = dependency.second;

        if (charVectorContains(chosenLetters, step) || charUnorderedSetContains(workingOn, step)) {
            continue;
        }

        if (neededSteps.empty()) {
            available.insert(step);
            continue;
        }

        for (const char &neededStep : neededSteps) {
            if (!charVectorContains(chosenLetters, neededStep)) {
                goto nextDependency;
            }
        }
        available.insert(step);

        nextDependency:
        {};
    }
    return available;
}

class Worker {

private:
    char workingOn;
    int secondsLeft;
    bool working;

public:
    Worker() {
        this->workingOn = '-';
        this->secondsLeft = 0;
        this->working = false;
    }

    void work() {
        this->secondsLeft--;
        if (this->secondsLeft <= 0) {
            this->working = false;
        }
    }

    void workOn(const char &c) {
        this->workingOn = c;
        this->secondsLeft = 60 + ((int) c % 32);
        this->working = true;
    }

    void resetWorkingOn() {
        this->workingOn = '-';
        this->secondsLeft = 0;
    }

    char getWorkingOn() const {
        return this->workingOn;
    }

    bool isWorking() const {
        return this->working;
    }
};

void day7() {
    ifstream inputFile("day7.txt");
    if (inputFile.is_open()) {
        unordered_map<char, vector<char>> dependencies;
        unordered_set<char> stepsNeeded;
        for (string dependencyString; getline(inputFile, dependencyString);) {
            char step, stepNeeded;
            sscanf(dependencyString.c_str(), "Step %c must be finished before step %c can begin.", &stepNeeded, &step);

            if (!dependencies.count(step)) {
                vector<char> v;
                dependencies[step] = v;
            }
            dependencies[step].push_back(stepNeeded);

            stepsNeeded.insert(stepNeeded);
        }

        for (const char &stepNeeded : stepsNeeded) {
            if (!dependencies.count(stepNeeded)) {
                vector<char> v;
                dependencies[stepNeeded] = v;
            }
        }

        char finalLetter = '-';
        for (const auto &dependency : dependencies) {
            char letter = dependency.first;
            if (stepsNeeded.find(letter) == stepsNeeded.end()) {
                finalLetter = letter;
            }
        }

        // Part 1
        vector<char> chosenLetters;
        while (chosenLetters.empty() || chosenLetters.back() != finalLetter) {
            set<char> available = getAvailableSteps(dependencies, chosenLetters);
            chosenLetters.push_back(*available.begin());
        }

        for (const char &letter : chosenLetters) {
            cout << letter;
        }
        cout << endl;

        // Part 2
        chosenLetters.clear();
        int second = -1;
        vector<Worker> workers;
        int workersCount = 5;
        for (int i = 0; i < workersCount; i++) {
            workers.emplace_back();
        }
        unordered_set<char> workingOn;
        while (chosenLetters.empty() || chosenLetters.back() != finalLetter) {
            set<char> available = getAvailableSteps(dependencies, chosenLetters, workingOn);
            for (Worker &worker : workers) {
                if (!worker.isWorking()) {
                    char done = worker.getWorkingOn();
                    if (done != '-' && !charVectorContains(chosenLetters, done)) {
                        chosenLetters.push_back(done);
                        workingOn.erase(done);
                        worker.resetWorkingOn();
                        available = getAvailableSteps(dependencies, chosenLetters, workingOn);
                    }
                }
            }

            for (Worker &worker : workers) {
                if (!worker.isWorking()) {
                    if (!available.empty()) {
                        char workOnNow = *available.begin();
                        worker.workOn(workOnNow);
                        workingOn.insert(workOnNow);
                        available.erase(available.begin());
                    }
                }
                if (worker.isWorking()) {
                    worker.work();
                }
            }
            second++;
        }
        cout << second << endl;
    }
}

int metadataSum(vector<int> &nodeData) {
    int totalMetadata = 0;

    int childNodeQuantity = nodeData[0], metadataEntryQuantity = nodeData[1];
    nodeData.erase(nodeData.begin(), nodeData.begin() + 2);

    for (int i = 0; i < childNodeQuantity; i++) {
        totalMetadata += metadataSum(nodeData);
    }

    for_each(nodeData.begin(), nodeData.begin() + metadataEntryQuantity, [&](int entry) {
        totalMetadata += entry;
    });
    nodeData.erase(nodeData.begin(), nodeData.begin() + metadataEntryQuantity);

    return totalMetadata;
}

int valueSum(vector<int> &nodeData) {
    int totalValue = 0;

    int childNodeQuantity = nodeData[0], metadataEntryQuantity = nodeData[1];
    nodeData.erase(nodeData.begin(), nodeData.begin() + 2);

    int childNodeValues[childNodeQuantity];
    for (int i = 0; i < childNodeQuantity; i++) {
        childNodeValues[i] = valueSum(nodeData);
    }

    for_each(nodeData.begin(), nodeData.begin() + metadataEntryQuantity, [&](int entry) {
        if (childNodeQuantity == 0) {
            totalValue += entry;
        } else {
            entry--;
            if (entry < childNodeQuantity) {
                totalValue += childNodeValues[entry];
            }
        }
    });
    nodeData.erase(nodeData.begin(), nodeData.begin() + metadataEntryQuantity);

    return totalValue;
}

void day8() {
    ifstream inputFile("day8.txt");
    if (inputFile.is_open()) {
        string nodeString;
        getline(inputFile, nodeString);
        inputFile.close();

        stringstream nodeStream(nodeString);
        vector<int> nodeData((istream_iterator<int>(nodeStream)), istream_iterator<int>());
        vector<int> nodeData2 = nodeData;

        // Part 1
        cout << metadataSum(nodeData) << endl;

        // Part 2
        cout << valueSum(nodeData2) << endl;
    }
}

long long maxScoreInElfGame(const long long &playerAmount, const long long &lastMarbleWorth) {
    long long playerScores[playerAmount];
    fill(playerScores, playerScores + playerAmount, 0);

    list<long long> marbles{0};

    auto next = [&](auto it) {
        if (++it == marbles.end())
            return marbles.begin();
        return it;
    };

    auto prev = [&](auto it) {
        if (it == marbles.begin())
            it = marbles.end();
        return --it;
    };

    auto currentMarble = marbles.begin();

    for (long long marble = 1; marble <= lastMarbleWorth; marble++) {
        if (marble % 23 == 0) {
            auto marbleToBeRemoved = prev(prev(prev(prev(prev(prev(prev(currentMarble)))))));
            playerScores[(marble - 1) % playerAmount] += marble + *marbleToBeRemoved;
            currentMarble = marbles.erase(marbleToBeRemoved);
        } else {
            currentMarble = marbles.insert(next(next(currentMarble)), marble);
        }
    }

    return *max_element(playerScores, playerScores + playerAmount);
}

void day9() {
    ifstream inputFile("day9.txt");
    if (inputFile.is_open()) {
        string gameInfoString;
        getline(inputFile, gameInfoString);
        inputFile.close();

        long long playerAmount, lastMarbleWorth;
        sscanf(gameInfoString.c_str(), "%lld players; last marble is worth %lld points", &playerAmount,
               &lastMarbleWorth);

        // Part 1
        cout << maxScoreInElfGame(playerAmount, lastMarbleWorth) << endl;

        // Part 2
        cout << maxScoreInElfGame(playerAmount, lastMarbleWorth * 100) << endl;
    }
}

struct Light {
    int x, y, vx, vy;
};

void day10() {
    ifstream inputFile("day10.txt");
    if (inputFile.is_open()) {
        vector<struct Light> lights;
        for (string dependencyString; getline(inputFile, dependencyString);) {
            struct Light l{};
            sscanf(dependencyString.c_str(), "position=<%d, %d> velocity=<%d, %d>", &l.x, &l.y, &l.vx, &l.vy);

            lights.push_back(l);
        }

        // Part 1
        int maximumSecondAmount = INT_MIN;
        for (const Light &l : lights) {
            int xSecondAmount = abs(l.x / l.vx), ySecondAmount = abs(l.y / l.vy);
            if (xSecondAmount > maximumSecondAmount) {
                maximumSecondAmount = xSecondAmount;
            }
            if (ySecondAmount > maximumSecondAmount) {
                maximumSecondAmount = ySecondAmount;
            }
        }
        maximumSecondAmount += 500;

        vector<struct Light> movedLights(lights);
        pair<long, int> minAreaForSeconds = make_pair(LONG_MAX, -1);
        for (int second = 1; second <= maximumSecondAmount; second++) {
            long minX = LONG_MAX, minY = LONG_MAX, maxX = LONG_MIN, maxY = LONG_MIN;
            for (Light &l : movedLights) {
                l.x += l.vx;
                if (l.x < minX) {
                    minX = l.x;
                } else if (l.x > maxX) {
                    maxX = l.x;
                }

                l.y += l.vy;
                if (l.y < minY) {
                    minY = l.y;
                } else if (l.y > maxY) {
                    maxY = l.y;
                }
            }

            long area = abs(maxX - minX) * abs(maxY - minY);
            if (area < minAreaForSeconds.first) {
                minAreaForSeconds = make_pair(area, second);
            }
        }

        vector<struct Light> finalLights(lights);
        for (int i = 0; i < minAreaForSeconds.second; i++) {
            for (Light &l : finalLights) {
                l.x += l.vx;
                l.y += l.vy;
            }
        }

        int minX = INT_MAX, minY = INT_MAX, maxX = INT_MIN, maxY = INT_MIN;
        for (const Light &l : finalLights) {
            if (l.x < minX) {
                minX = l.x;
            } else if (l.x > maxX) {
                maxX = l.x;
            }
            if (l.y < minY) {
                minY = l.y;
            } else if (l.y > maxY) {
                maxY = l.y;
            }
        }

        vector<vector<char>> field((unsigned long) (maxY - minY + 1),
                                   vector<char>((unsigned long) (maxX - minX + 1), '.'));
        for (const Light &l : finalLights) {
            field[l.y - minY][l.x - minX] = '#';
        }
        for (const vector<char> &line : field) {
            for (const char &c : line) {
                cout << c;
            }
            cout << endl;
        }

        // Part 2
        cout << minAreaForSeconds.second << endl;
    }
}

int maxSubarraySum(const vector<int> &v, const int &width, int &startIndex, int &endIndex) {
    int maxSumEndingHere = v[0], maxSum = v[0];
    int lastIndex = 0;
    bool foundSubarray = false;
    for (int i = 1; i < v.size(); i++) {
        if (maxSumEndingHere + v[i] > v[i]) {
            maxSumEndingHere += v[i];
        } else {
            maxSumEndingHere = v[i];
            lastIndex = i;
        }
        if (maxSumEndingHere > maxSum && i + 1 - lastIndex == width) {
            maxSum = maxSumEndingHere;
            startIndex = lastIndex;
            endIndex = i + 1;
            foundSubarray = true;
        }
    }
    return foundSubarray ? maxSum : INT_MIN;
}

void day11() {
    ifstream inputFile("day11.txt");
    if (inputFile.is_open()) {
        int gridSerialNumber;
        inputFile >> gridSerialNumber;
        inputFile.close();

        unsigned long gridSize = 300;
        vector <vector<int>> grid(gridSize, vector<int>(gridSize, 0));
        for (int i = 0, x = 1; i < gridSize; i++, x++) {
            for (int j = 0, y = 1; j < gridSize; j++, y++) {
                int rackID = x + 10;
                int powerLevel = rackID * y;
                powerLevel += gridSerialNumber;
                powerLevel *= rackID;
                string pl = to_string(powerLevel);
                powerLevel = pl.length() >= 3 ? pl[pl.length() - 3] - '0' : 0;
                powerLevel -= 5;
                grid[i][j] = powerLevel;
            }
        }

        // Part 1
        int largestTotalPower = INT_MIN;
        pair<int, int> coordinates;
        for (int i = 0, x = 1; i < gridSize - 2; i++, x++) {
            for (int j = 0, y = 1; j < gridSize - 2; j++, y++) {
                int totalPower = 0;
                for (int p = i; p < i + 3; p++) {
                    for (int q = j; q < j + 3; q++) {
                        totalPower += grid[p][q];
                    }
                }
                if (totalPower > largestTotalPower) {
                    largestTotalPower = totalPower;
                    coordinates = make_pair(x, y);
                }
            }
        }
        cout << coordinates.first << "," << coordinates.second << endl;

        // Part 2
        /* With Kadane's algorithm in 2D -> not working properly

        int maxSum = INT_MIN, maxLeftIndex = -1, maxRightIndex = -1, maxUpIndex = -1, maxDownIndex = -1;
        for (int leftIndex = 0; leftIndex < gridSize; leftIndex++) {
            vector<int> rangeSum(gridSize, 0);
            int width = 0;
            for (int rightIndex = leftIndex; rightIndex < gridSize; rightIndex++) {
                width++;
                for (int i = 0; i < gridSize; i++) {
                    rangeSum[i] += grid[rightIndex][i];
                }
                int upIndex = 0, downIndex = 1, currentSum = maxSubarraySum(rangeSum, width, upIndex, downIndex);
                if (currentSum > maxSum) {
                    maxSum = currentSum;
                    maxLeftIndex = leftIndex;
                    maxRightIndex = rightIndex + 1;
                    maxUpIndex = upIndex;
                    maxDownIndex = downIndex;
                }
            }
        }
        cout << maxLeftIndex + 1 << "," << maxUpIndex + 1 << "," << maxRightIndex - maxLeftIndex << endl;
        */

        // https://en.wikipedia.org/wiki/Summed-area_table
        vector <vector<int>> summedGrid(gridSize, vector<int>(gridSize, 0));
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                if (i == 0 && j == 0) {
                    summedGrid[0][0] = grid[0][0];
                } else if (i == 0) {
                    summedGrid[0][j] = grid[i][j] + summedGrid[i][j - 1];
                } else if (j == 0) {
                    summedGrid[i][0] = grid[i][j] + summedGrid[i - 1][j];
                } else {
                    summedGrid[i][j] =
                            grid[i][j] + summedGrid[i][j - 1] + summedGrid[i - 1][j] - summedGrid[i - 1][j - 1];
                }
            }
        }

        largestTotalPower = INT_MIN;
        coordinates = make_pair(-1, -1);
        int largestSquareSize = 0;
        for (int squareSize = 1; squareSize <= gridSize; squareSize++) {
            for (int x = 0; x < gridSize + 1 - squareSize; x++) {
                for (int y = 0; y < gridSize + 1 - squareSize; y++) {
                    int A = summedGrid[x][y],
                            B = summedGrid[x + squareSize - 1][y],
                            C = summedGrid[x][y + squareSize - 1],
                            D = summedGrid[x + squareSize - 1][y + squareSize - 1];
                    int totalPower = D - B - C + A;
                    if (totalPower > largestTotalPower) {
                        largestTotalPower = totalPower;
                        coordinates = make_pair(x + 2, y + 2); // strange offset for x, y and squareSize.. but it works!
                        largestSquareSize = squareSize - 1;
                    }
                }
            }
        }
        cout << coordinates.first << "," << coordinates.second << "," << largestSquareSize << endl;
    }
}

int main() {
    //day1();
    //day2();
    //day3();
    //day4();
    //day5();
    //day6();
    //day7();
    //day8();
    //day9();
    //day10();
    day11();
    return 0;
}